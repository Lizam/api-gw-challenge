# api-gw-challenge

## To Run
aws apigateway test-invoke-method --rest-api-id $API_ID --resource-id $RESOURCE_ID --http-method POST --path-with-query-string "" --body file://input.json

## Improvements
- Use KMS to encrypt Lambda environment variables